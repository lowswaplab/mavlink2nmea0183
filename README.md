# mavlink2nmea0183

Receive data from a [MAVLink](https://mavlink.io/) device and output NMEA0183 sentences
(e.g. for gpsd).

This is a work in progress!

mv2nm outputs NMEA0183 sentences to
[gpsd](https://gpsd.gitlab.io/gpsd/index.html).

mv2nm also outputs JSON via MQTT for use with
[node-red-contrib-web-worldmap](https://www.npmjs.com/package/node-red-contrib-web-worldmap).

Source code can be found on the
[mavlink2nmea0183 GitLab
page](https://gitlab.com/lowswaplab/mavlink2nmea0183).

[Low SWaP Lab](https://www.lowswaplab.com/)

